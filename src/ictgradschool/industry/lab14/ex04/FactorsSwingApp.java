package ictgradschool.industry.lab14.ex04;

import ictgradschool.industry.lab14.examples.example03_flipper.Flipper;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Simple application to calculate the prime factors of a given number N.
 * 
 * The application allows the user to enter a value for N, and then calculates 
 * and displays the prime factors. This is a very simple Swing application that
 * performs all processing on the Event Dispatch thread.
 *
 */
public class FactorsSwingApp extends JPanel implements ActionListener{

	private JButton _startBtn;        // Button to start the calculation process.
	private JButton _abortBtn;
	private JTextArea _factorValues;  // Component to display the result.
	private final JTextField tfN;
	private primeWorker prime;



	private class primeWorker extends SwingWorker<Void, Long> {

		long n;


		public  primeWorker(long number){
			this.n = number;
		}

		@Override
		public Void doInBackground() {
			//List<Long> numbers = new ArrayList<Long>();
				for (long i = 2; i * i <= n && !prime.isCancelled(); i++) {

					// If i is a factor of N, repeatedly divide it out
					while (n % i == 0 && !prime.isCancelled()) {
						publish(i);
						n = n / i;
					}
				}
				if (n > 1 && !prime.isCancelled()) {
					publish(n);
				}

			return null;
			// if biggest factor occurs only once, n > 1

		}

		@Override
		public void done() {
			//List<Long> numbers = null;

			_startBtn.setEnabled(true);
			_abortBtn.setEnabled(false);

			// Restore the cursor.
			setCursor(Cursor.getDefaultCursor());
			/*if(!prime.isCancelled()) {

				try {
					numbers = get();
					for (Long number : numbers) {
						_factorValues.append(number + "\n");
					}
				} catch (InterruptedException e) {

				} catch (ExecutionException e) {

				}
			}*/
			// Re-enable the Start button.
		}



		@Override
		protected void process(List<Long> numbers){
			//List<Long> num = numbers;
			for ( Long num: numbers)
			_factorValues.append(Long.toString(num)+"\n");
		}

	}





	public FactorsSwingApp() {
		// Create the GUI components.
		JLabel lblN = new JLabel("Value N:");
		tfN = new JTextField(20);

		_startBtn = new JButton("Compute");
		_abortBtn = new JButton("Cancel");
		_abortBtn.setEnabled(false);
		_factorValues = new JTextArea();
		_factorValues.setEditable(false);

		// Add an ActionListener to the start button. When clicked, the
		// button's handler extracts the value for N entered by the user from
		// the textfield and find N's prime factors.
		_startBtn.addActionListener(this);
		_abortBtn.addActionListener(this);


		// Construct the GUI.
		JPanel controlPanel = new JPanel();
		controlPanel.add(lblN);
		controlPanel.add(tfN);
		controlPanel.add(_startBtn);
		controlPanel.add(_abortBtn);

		JScrollPane scrollPaneForOutput = new JScrollPane();
		scrollPaneForOutput.setViewportView(_factorValues);

		setLayout(new BorderLayout());
		add(controlPanel, BorderLayout.NORTH);
		add(scrollPaneForOutput, BorderLayout.CENTER);
		setPreferredSize(new Dimension(500,300));
	}


	public void actionPerformed(ActionEvent event)  {


		if(event.getSource() == _startBtn)
		{
			String strN = tfN.getText().trim();
			long n = 0;

			try {
				n = Long.parseLong(strN);
			} catch(NumberFormatException e) {
				System.out.println(e);
			}

			// Disable the Start button until the result of the calculation is known.
			_startBtn.setEnabled(false);
			_abortBtn.setEnabled(true);

			// Clear any text (prime factors) from the results area.
			_factorValues.setText(null);

			// Set the cursor to busy.
			setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

			// Start the computation in the Event Dispatch thread.
			prime = new primeWorker(n);
			prime.execute();
		}
		else if(event.getSource() == _abortBtn){
			prime.cancel(true);
			// Re-enable the Start button.
			_startBtn.setEnabled(true);
			_abortBtn.setEnabled(false);

			// Restore the cursor.
			setCursor(Cursor.getDefaultCursor());
			prime = null;
			tfN.setText("");
		}
	}

	private static void createAndShowGUI() {
		// Create and set up the window.
		JFrame frame = new JFrame("Prime Factorisation of N");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Create and set up the content pane.
		JComponent newContentPane = new FactorsSwingApp();
		frame.add(newContentPane);

		// Display the window.
		frame.pack();
        frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		// Schedule a job for the event-dispatching thread:
		// creating and showing this application's GUI.
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}

