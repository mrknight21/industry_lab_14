package ictgradschool.industry.lab14.ex05;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.concurrent.ExecutionException;

/**
 * A shape which is capable of loading and rendering images from files / Uris / URLs / etc.
 */
public class ImageShape extends Shape {

    private Image image = null;

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, String fileName) throws MalformedURLException {
        this(x, y, deltaX, deltaY, width, height, new File(fileName).toURI());
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, URI uri) throws MalformedURLException {
        this(x, y, deltaX, deltaY, width, height, uri.toURL());
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, URL url) {
        super(x, y, deltaX, deltaY, width, height);
        imageworker imagework = new imageworker(url, width, height);
        imagework.execute();

    }



    private class imageworker extends SwingWorker<Image, Void>{
        URL link;
        int scaleWidth;
        int scaleHeight;

        public imageworker (URL url, int width, int height){
            this.link = url;
            this.scaleWidth = width;
            this.scaleHeight = height;
        }

        @Override
        public Image doInBackground() {
            Image image = null;
            try {
                image = ImageIO.read(link);
                int imgWidth = image.getWidth(null);
                int imgHeight = image.getHeight(null);
                if (scaleWidth == imgWidth && scaleHeight == imgHeight) {
                     return image;
                } else {
                    image = image.getScaledInstance(scaleWidth, scaleHeight, Image.SCALE_SMOOTH);
                    int randomwidth = image.getWidth(null);
                    int randomheight = image.getHeight(null);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return image;
        }

        @Override
        public void done(){
            Image tempimage = null;
            try {
                tempimage = get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            ImageShape.this.image = tempimage;
        }








        }


    @Override
    public void paint(Painter painter) {



        if (this.image == null)
        {

            painter.setColor(Color.GRAY);
            painter.fillRect(fX,fY,fWidth,fHeight);
            painter.setColor(Color.red);
            painter.drawCenteredText("Loading......", fX, fY, fWidth, fHeight);
            painter.drawRect(fX,fY,fWidth,fHeight);
        }
        else {


            painter.drawImage(this.image, fX, fY, fWidth, fHeight);
        }
    }
}
