package ictgradschool.industry.lab14.trial;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by mche618 on 9/05/2017.
 */
public class Swingworkerss implements ActionListener {
    private JLabel progressLabel = "";
    private JLabel myLabel = …;
    private JButton myButton = …;
    /** Called when the button is clicked. */
    @Override
    public void actionPerformed(ActionEvent e) {
        myButton.setEnabled(false);
// Start the SwingWorker running
        MySwingWorker worker = new MySwingWorker();
        worker.execute();
// When the SwingWorker has finished, display the result in myLabel.

    }


    private class MySwingWorker extends SwingWorker<Integer, Void> {

        protected int doInBackground() throws Exception {
            int result = 0;
            for (int i = 0; i < 100; i++) {

            result += doStuffAndThings();
// Report intermediate results
            progressLabel.setText("Progress: " + i + "%");
        }
return result;
    }
}
}